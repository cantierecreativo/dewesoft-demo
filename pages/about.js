import Link from 'next/link';

export default function About({ greeting }) {
  return (
    <div className="w-screen min-h-screen flex flex-col my-10 items-center">
      <h1 className="text-5xl font-bold">{greeting}</h1>
      <Link href="/">
        <a>Home</a>
      </Link>
    </div>
  );
}
export async function getStaticProps({ locale }) {
  const message =
    locale === 'sl' ? 'Zdravo' : locale === 'it' ? 'Ciao' : 'Hello';
  return {
    props: {
      greeting: `${message}  - "${locale}"`,
    },
  };
}
