import Link from 'next/link';
import Head from 'next/head';

import pages from 'scripts/nestedRoutes';
import Langs from 'components/Langs';

function Page({ url, title, descr }) {
  return (
    <div className="w-screen min-h-screen flex flex-col my-10 px-20">
      <Head>
        <title>{title}</title>
        <meta name="description" content={descr} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Langs />
      <div className="text-xl font-bold my-4"> PAGE</div>
      <div className="my-4">
        <label>Title:</label>
        <h1 className="text-md font-bold"> {title}</h1>
      </div>
      <div className="my-4">
        <label>Parts:</label>
        <div className="text-sm">{descr}</div>
      </div>
      <div className="my-4">
        <label>PATH:</label>
        <div className="text-sm">/{url}</div>
      </div>
      <div className="my-4"></div>
      <Link href="/">
        <a>Home</a>
      </Link>
    </div>
  );
}

export async function getStaticPaths() {
  const paths = pages.map((item) => ({
    params: { slug: item.slugs },
    locale: item.locale,
  }));
  // console.log('paths', paths);
  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const url = `${params.slug.join('/')}`;
  const title = params.slug[params.slug.length - 1];
  const descr = params.slug.join(' - ');
  return { props: { url, title, descr } };
}

export default Page;
