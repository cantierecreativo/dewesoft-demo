import Link from 'next/link';
import { useState, useEffect } from 'react';

// import { pages } from 'data/fake';
import allPages from 'scripts/nestedRoutes';
import { useRouter } from 'next/router';

const Pages = () => {
  const router = useRouter();

  const [maxDepth, setMaxDepth] = useState(1);
  const [pages, setPages] = useState([]);
  const { locale } = router;

  useEffect(() => {
    if (locale) {
      const pages = allPages.filter((p) => p.locale == locale);
      setMaxDepth(
        pages.reduce((max, r) => {
          return Math.max(max, r.slugs.length);
        }, 1)
      );
      setPages(pages);
    }
  }, [locale]);

  return (
    <div className="my-10">
      <h4>LINKS</h4>
      <h3>{`results ${pages.length},  locale ${locale}, max depth ${maxDepth}`}</h3>
      <div className="my-5 flex-col">
        <div className="px-2 ">
          <Link href="/about">
            <a className="text-pink-400">About</a>
          </Link>
        </div>
        {pages.map((page) => {
          return (
            <div key={page.slug} className="px-2">
              <Link href={`/${page.slugs.join('/')}`} locale={page.locale}>
                <a>{page.slugs.join(' - ')}</a>
              </Link>
              <small>{` - depth ${page.slugs.length}`}</small>
            </div>
          );
        })}
      </div>
    </div>
  );
};
// .filter((p) => p.locale === locale)
export default Pages;
