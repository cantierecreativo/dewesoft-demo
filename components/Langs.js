import Link from 'next/link';
import { useRouter } from 'next/router';

const Langs = () => {
  const router = useRouter();
  const { locale } = router;
  return (
    <div className="my-5 flex">
      <div className="px-10 text-blue-400">{locale}</div>

      {router?.locales
        ?.filter((l) => l != locale)
        .map((l) => {
          return (
            <div key={l} className="px-2">
              <Link href="/" locale={l}>
                <a>{l}</a>
              </Link>
            </div>
          );
        })}
    </div>
  );
};

export default Langs;
