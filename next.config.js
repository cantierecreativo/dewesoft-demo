module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: ['en-US', 'br', 'de', 'es', 'fr', 'it', 'ru', 'se', 'sl'],
    defaultLocale: 'en-US',
  },
};
