const parser = require('fast-xml-parser');
const fs = require('fs');
const path = require('path');
const fileNamePath = path.join(__dirname, './sitemap.xml');
const outFile = path.join(__dirname, './data.json');
const xmlData = fs.readFileSync(fileNamePath).toString();

const options = {};
let jsonObj;
if (parser.validate(xmlData) === true) {
  //optional (it'll return an object in case it's not valid)
  jsonObj = parser.parse(xmlData, options);
}

// Intermediate obj
const tObj = parser.getTraversalObj(xmlData, options);
jsonObj = parser.convertToJson(tObj, options);

jsonObj = parser.convertToJson(tObj, options);
const list = jsonObj.urlset.url
  .map((u) => u.loc)
  .filter((u) => u.indexOf('https://dewesoft.com/') === 0);
fs.writeFileSync(outFile, JSON.stringify(list.sort(), null, 2));
