const fs = require('fs');
const path = require('path');

const routes = require('./routes.json');

const locales = ['br', 'de', 'es', 'fr', 'it', 'ru', 'se', 'sl'];
const data = routes.map((r) => {
  withoutPrefix = r.slice('https://dewesoft.com/'.length);
  const split = withoutPrefix.split('/');
  const locale = locales.includes(split[0]) ? split[0] : 'en-US';
  let slugs = split;
  if (locale !== 'en-US') {
    slugs = slugs.slice(1);
  }
  return { slugs, locale };
});
// console.log(JSON.stringify(data, null, 2));

const cleanup = data.slice(1).filter((o) => o.slugs.length !== 0 && o.locale);

const outFile = path.join(__dirname, './nestedRoutes.json');
fs.writeFileSync(outFile, JSON.stringify(cleanup, null, 2));
